# Linux Sandboxing

*https://gitlab.com/cvoges12/sandbox-talk*

![](images/penguin-using-sandy-computer.png)

## Introduction

| Ken | Midi |
|-----|------|
| Software dev @ ShorebreakSecurity.com | CEO / Software Dev / Consultant <br> @ Vojes Enterprises |
| Python, JS, Go, C#, Java | Py, C/C++, Java, Ada, Forth,<br>Haskell, Lisp, Erlang/Elixir,<br>Rust, Zig, and much more :D |
| https://VoidNet.tech<br>https://mstdn.party/kevinf | https://gitlab.com/cvoges12 |

## Introduction


The fragmented world of linux sandboxing software

<aside class="notes">
- This talk does not cover:
  * Proprietary solutions (at least not too many)
  * Other operating systems (in depth)
  * Other security tools, such as:
    + Firewalls
    + Anti-virus
    + Mandatory Access Control, ie:
      - SELinux
      - AppArmor
    + Etc  
</aside>


# What is sandboxing?

Sandboxing gives each program or set of programs their own isolated environment.

Sandboxed programs have reduced access/insight into one's system


# What is sandboxing?

![](https://i.stack.imgur.com/BfF5o.gif)


# Why Sandboxing?

## Before Sandboxing

* Pre-1960s
* Single-user systems
* Programs had substantial or entire control over the whole OS
* Installing software was not convenient or simple
* Smaller attack surface

## Today

* Multi-user systems
* Multi-processing systems
* Internet is wide-spread
* Software can't be trusted
* Installing software happens often
* Large attack surface
* You use sandboxing everyday (IOS, Android, ChromeOS, browsers)

<aside class="notes">
- Sandboxing is why said platforms don't need AV
</aside>

## So now what?

![](images/cat-sandbox.png)

# Chroot

## Chroot

* 1979 - Version 7 Unix
* Stands for "change-root"
* Only limits file system access
* No isolation of memory, cpu, networking, etc

![](images/chroot-command.jpg)

# Jails

## FreeBSD Jails

* 2000 - FreeBSD `jail()` kernel syscall
* Not available on Linux
* Limited access to file system, processes, network stack, mount table
* By default, no raw socket access
* Isolated users and superuser accounts
* Shared kernel

## Firejail

* SUID app
* Uses Linux namespaces and `seccomp-bpf()`
  * available in all Linux kernels v3.x or higher
* Limited access to file system, processes, network stack, mount table
* Shared kernel
* Essentially FreeBSD jails on Linux

## Bubblewrap and Bubblejail

* Bubblewrap is similar to firejail
* Not SUID app
* Bubblewrap has no premade profiles
* Bubblejail is a wrapper around bubblewrap with application profiles

# System Virtual Machines

## What?

Most commonly known as just "Virtual Machines" or "VMs"

![](images/vm-simple.ppm)

## First VM

* 1960s
* IBM developed the first VM (CP-40)
* Likely not made with security in mind
![ibm system 360](images/IBM_System_360_at_USDA.jpg)

## Types of Virtual Machines

![](images/Hyperviseur.png)

## Type 2

![](images/hypervisor-type2.png)

## Type 2 Choices

* VirtualBox
* VMWare Workstation (proprietary)

## Type 2 Pros / Cons

* Pros
  * Convenience
  * Easy
* Cons
  * Performance
  * Security

## Type 1

![](images/hypervisor-type1.png)

## Type 1 Choices

* Xen:
  - Qubes OS
* KVM and KVM based solutions:
  - GNOME box
  - oVirt
  - QEMU/KVM

## KVM and QEMU

![](images/qemu-infographic.png)

## Type 1 Pros / Cons

* Pros
  * Performance
  * Security
* Cons
  * Not convenience
  * Not easy

# Process VM

* Unrelated to System VMs
* Stuff like JVM, BEAM, etc


# QubesOS

* Best default desktop linux sandboxing experience
* Efficient, seamless & secure (for x11)
* Fair learning curve
* Overhead
* GPU hell

## QubesOS

![](images/qubes-desktop.jpg)

# Containers

## Containers

![](images/containers.jpg)

## Containerd

![](images/containerd.png)

![](images/containerd-diagram.png)

## LXC / Docker

![](images/docker.png)

## Podman

* Not mainly for security sandboxing
* `seccomp`, `cap-drop` can help
* rootless by default

# Browser sandboxes

Browsers usually implement sandboxes through a series of process sandboxes and IPC brokers

# Android

* Unique UID per app
* SELinux, `seccomp`, and limited filesystem

# ChromeOS sandbox

* Chrome, Android, and Linux apps each have their own VM
* Similar to Android sandboxing
* Inside the Chrome VM, minijail is used to sandbox applications. (seccomp, namespaces, capabilities)
* Arguably most secure consumer OS, comparable to IOS

# Conclusion

Slides availble at:

*https://gitlab.com/cvoges12/sandbox-talk*

**Questions?**


<script src="plugin/notes/notes.js"></script>
<script>
    Reveal.initialize({
        plugins: [ RevealNotes ]
    });
</script>
